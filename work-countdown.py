import time
import pystray
from pystray import Menu, MenuItem
from PIL import Image
import threading
from datetime import datetime
from dateutil import tz
import tkinter as tk
from tkinter import PhotoImage
from win10toast import ToastNotifier
from pynput.mouse import Listener as MouseListener
from pynput.keyboard import Listener as KeyboardListener

breaks = []


def add_break(start_time, pause_length):
    global breaks
    start_time_text = seconds_to_time(start_time)
    pause_length_text = seconds_to_time_length(pause_length)

    breaks.append([start_time_text + " " + pause_length_text, pause_length])


def seconds_to_time_length(seconds):
    return str(
        datetime.utcfromtimestamp(seconds).strftime("%H hours, %M minutes, %S seconds")
    )


def seconds_to_time(seconds):
    return str(datetime.fromtimestamp(seconds).astimezone().strftime("%I:%M %p"))


def show_window():
    global breaks, start_time, timer_length, total_elapsed_time
    # Code to open a window goes here
    # Create the main window
    root = tk.Tk()
    root.title("Work Countdown")
    photo = PhotoImage(file="icon.png")
    root.iconphoto(False, photo)

    settings_frame = tk.Frame(root)
    settings_frame.pack()

    start_time_text = seconds_to_time(start_time)
    label = tk.Label(settings_frame, text="Start Time: " + start_time_text)
    label.pack()

    finish_time = timer_length - total_elapsed_time + time.time()
    finish_time_text = seconds_to_time(finish_time)
    label = tk.Label(settings_frame, text="Est. Finish Time: " + finish_time_text)
    label.pack()

    label = tk.Label(settings_frame, text="Change the time remaining:")
    label.pack()

    # Add hours text box
    hrs_text_box = tk.Entry(settings_frame)
    hrs_text_box.insert(0, "Hours")
    hrs_text_box.pack(side=tk.LEFT)

    # Add mins text box
    mins_text_box = tk.Entry(settings_frame)
    mins_text_box.insert(0, "Minutes")
    mins_text_box.pack(side=tk.LEFT)

    # Add a button
    button = tk.Button(settings_frame, text="Update")

    def update_button_click(event):
        global timer_length, start_time, pause_start_time
        # This function will be called when the button is clicked
        hrs = int(hrs_text_box.get())
        mins = int(mins_text_box.get())

        timer_length = hrs * 60 * 60 + mins * 60 + total_elapsed_time
        root.destroy()

    # Bind the button click event to the callback function
    button.bind("<Button-1>", update_button_click)
    button.pack()

    break_history_frame = tk.Frame(root)
    break_history_frame.pack()

    def remove_button_click(index):
        # Get the amount of time to remove from timer_length
        global timer_length
        break_length = breaks[index][1]
        timer_length -= break_length
        breaks.pop(index)
        root.destroy()
        show_window()

    # Show breaks
    for index, b in enumerate(breaks):
        break_item_frame = tk.Frame(break_history_frame)

        label = tk.Label(break_item_frame, text=b[0])
        label.pack(side=tk.LEFT)

        button = tk.Button(break_item_frame, text="Remove",command = lambda idx = index: remove_button_click(idx))
        button.pack(side=tk.RIGHT)

        break_item_frame.pack()

    # Run the tkinter event loop
    root.mainloop()


def create_menu(icon):
    def close_program():
        global listener, thread
        icon.stop()
        mouse_listener.stop()
        keyboard_listener.stop()
        thread.join()

    menu = Menu(
        MenuItem("Show", show_window, default=True), MenuItem("Quit", close_program)
    )
    icon.title = "Time remaining: 7hrs 40min"
    icon.menu = menu


def create_notification(text):
    toaster = ToastNotifier()
    toaster.show_toast(
        "Work Countdown", text, duration=5, icon_path="icon.ico", threaded=True
    )


def countdown(icon):
    global timer_length, start_time, pause_start_time, total_elapsed_time, mouse_listener, keyboard_listener
    # Set the timer to 7.5 hours (in seconds)
    timer_length = 7.5 * 60 * 60

    # Set the pause threshold to 5 minutes (in seconds)
    pause_threshold = 60 * 5

    # Start the countdown timer
    start_time = time.time()
    pause_start_time = start_time
    total_elapsed_time = 0

    def input_detected(arg1, arg2=0, arg3=0, arg4=0):
        global timer_length, start_time, pause_start_time, total_elapsed_time
        elapsed_time = time.time() - pause_start_time

        # If we haven't detected input for longer than the pause threshold, we consider this a break and add it to the timer length
        if elapsed_time > pause_threshold:
            timer_length += elapsed_time
            pause_length_text = seconds_to_time_length(elapsed_time)
            add_break(pause_start_time, elapsed_time)
            create_notification(f"Countdown resumed after {pause_length_text}")

        # Print the remaining time on the countdown timer
        remaining_text = seconds_to_time_length(timer_length - total_elapsed_time)
        finish_time = timer_length - total_elapsed_time + time.time()
        finish_time_text = seconds_to_time(finish_time)
        icon.title = f"{remaining_text} remaining. Finish time: {finish_time_text}"

        # Reset the pause start time
        pause_start_time = time.time()

        # Calculate total elapsed time
        total_elapsed_time = time.time() - start_time
        if timer_length < total_elapsed_time:
            icon.stop()
            return False  # Stop the listener

    keyboard_listener = KeyboardListener(
        on_press=input_detected, on_release=input_detected
    )
    mouse_listener = MouseListener(
        on_move=input_detected, on_click=input_detected, on_scroll=input_detected
    )

    # Start the threads and join them so the script doesn't end early
    keyboard_listener.start()
    mouse_listener.start()
    keyboard_listener.join()
    mouse_listener.join()

    create_notification("Time's up, work is over!")


def main():
    global thread
    create_notification("Have a good day of work!")
    icon = pystray.Icon("My Tray Icon")
    icon.icon = Image.open("icon.png")
    create_menu(icon)
    thread = threading.Thread(target=countdown, args=[icon])
    thread.start()
    icon.run()


if __name__ == "__main__":
    main()
