# Work Countdown
This is a simple python based app that uses the windows system tray and notifications to countdown your work day.

# Installation
Currently, installation is very manual. 

## Dependances
You will need the following:
* Python 3.X
* Dependencies as specified in requirements.txt (run `pip install -r requirements.txt`)

## Building
1. Use the `build.bat` file to create the executable which will be output to the `dist/` folder.
2. Create a shortcut in the "Start Up" folder for your user account which can be found by pressing Win + R and running `shell:startup`
3. Next time you log in, it will start automatically. This can be disabled in the Task Manager > Startup
